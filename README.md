## Preparando o projeto
Este projeto necessita apenas do [Node JS](https://nodejs.org/)  `~3.10.8`.

## Intalando o projeto
O repositório do projeto está no [Gitlab](https://gitlab.com/brunoflora/framework-material.git/)  `~0.0.1`.

Utilize os comandos abaixo para clonar o repositório e baixar todas as dependências do projeto:

```shell
git clone git@gitlab.com:brunoflora/framework-material.git
cd framework-material
npm install
npm install -g bower
```
Este projeto utiliza o [Grunt](http://gruntjs.com) `~0.4.5` para compilar o projeto, execute os comando abaixo para instala-lo:

```shell
npm install -g grunt-cli
```
## Estrutura do projeto

O projeto é modular para facilitar futuras implementações.
Para isso o projeto foi dividido na seguinte estrutura:

```
theme/              
├── html/           Componentes do tema (headers, menus etc)
├── img/            Imagens
├── js/             Scripts
├── less/           Estilos
└── index.html      Página de referência para os componentes
```

Utilizamos o Grunt para concatenar todas as dependências do front-end, compilar os arquivos em [LESS](http://lesscss.org), mover e renomear as fontes e criar um pré-visualização do tema.
Basta escrever o seguinte comando no terminal:

```shell
grunt build
```
Será gerado então um arquivo `preview.html` dentro da pasta `website_main\theme`.
